package br.ucsal.bes20182.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class MesaDao {

	private static final String MESA_NAO_ENCONTRADA = "Mesa n�o encontrada (n�mero = %d).";
	
	public List<Mesa> mesas = new ArrayList<>();

	public void incluir(Mesa mesa) {
		mesas.add(mesa);
	}

	public Mesa obterPorNumero(Integer numero) throws RegistroNaoEncontrado {
		for (Mesa mesa : mesas) {
			if (mesa.getNumero().equals(numero)) {
				return mesa;
			}
		}
		throw new RegistroNaoEncontrado(String.format(MESA_NAO_ENCONTRADA, numero));
	}

}

package br.ucsal.bes20182.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class ComandaDao {

	private static final String MENS_COMANDA_NAO_ENCONTRADA = "Comanda n�o encontrada (c�digo = %d).";
	
	public List<Comanda> comandas = new ArrayList<>();

	public void incluir(Comanda comanda) {
		comandas.add(comanda);
	}

	public Comanda obterPorCodigo(Integer codigo) throws RegistroNaoEncontrado {
		for (Comanda comanda : comandas) {
			if (comanda.getCodigo().equals(codigo)) {
				return comanda;
			}
		}
		throw new RegistroNaoEncontrado(String.format(MENS_COMANDA_NAO_ENCONTRADA, codigo));
	}

}

package br.ucsal.bes20182.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class ItemDao {

	private static final String ITEM_NAO_ENCONTRADO = "Item n�o encontrado (c�digo = %d).";
	
	public List<Item> itens = new ArrayList<>();

	public void incluir(Item item) {
		itens.add(item);
	}

	public Item obterPorCodigo(Integer codigo) throws RegistroNaoEncontrado {
		for (Item item : itens) {
			if (item.getCodigo().equals(codigo)) {
				return item;
			}
		}
		throw new RegistroNaoEncontrado(String.format(ITEM_NAO_ENCONTRADO, codigo));
	}

}

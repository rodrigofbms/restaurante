package br.ucsal.bes20182.testequalidade.restaurante.dubles;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaDAOMock {
	
	
	public Mesa obterPorNumero(Integer numero) {
		Mesa mesa = new Mesa(numero);
		mesa.setSituacao(SituacaoMesaEnum.LIVRE);
		return mesa;
		}
	
}

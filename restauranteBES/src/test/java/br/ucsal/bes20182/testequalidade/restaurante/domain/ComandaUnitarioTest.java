package br.ucsal.bes20182.testequalidade.restaurante.domain;

import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20182.testequalidade.restaurante.dubles.ComandaMock;
import org.junit.Assert;

public class ComandaUnitarioTest {

	// M�todo a ser testado: public Double calcularTotal() {
	// Verificar o c�lculo do valor total, com um total de 4 itens.
	// Obs: lembre-se de substituir o conte�do do atributo itens por uma lista
	// com 4 itens.
	private ComandaMock comandaMock;
	
	@Before
	public void setup() {
		comandaMock = new ComandaMock();

	}
	
	@Test
	public void calcularTotal4Itens() {
		Double atual = 40.0;
		Double esperado = comandaMock.calcularTotal();
		Assert.assertEquals(esperado, atual);
	}

}

package br.ucsal.bes20182.testequalidade.restaurante.dubles;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal.bes20182.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class ComandaMock {

	Integer codigo;
	public SituacaoComandaEnum situacao = SituacaoComandaEnum.ABERTA;
	Boolean exceptionChamado = false;
	public Map<Item, Integer> itens = new HashMap<>();
	
	

public ComandaMock(Mesa mesa) {
		
	}

public ComandaMock(Integer numero) {
	this.codigo = numero;
}


public ComandaMock() {
	
}

public Integer getCodigo() {
	return codigo;
}


public void setCodigo(Integer codigo) {
	this.codigo = codigo;
}

public void verificarSituacaoMesa(Mesa mesa) {
	if (mesa.getSituacao().equals(SituacaoMesaEnum.OCUPADA)) {
		
	}
}

public void verificarComandaLivre() throws ComandaFechadaExceptionMock {
	if (situacao.equals(SituacaoComandaEnum.FECHADA)) {
		exceptionChamado = true;
	}
}

public SituacaoComandaEnum getSituacao() {
	return situacao;
}


public void setSituacao(SituacaoComandaEnum situacao) {
	this.situacao = situacao;
}


public Boolean getExceptionChamado() {
	return exceptionChamado;
}


public void setExceptionChamado(Boolean exceptionChamado) {
	this.exceptionChamado = exceptionChamado;
}


public void incluirItem(Item item, Integer numero) throws ComandaFechadaExceptionMock {
	verificarComandaLivre();
	itens.put(item, numero);
}

public Double calcularTotal() {
	Item item1 = new Item("ma��", 10.0);
	Item item2 = new Item("mel�o", 10.0);
	Item item3 = new Item("abacaxi", 10.0);
	Item item4 = new Item("mam�o", 10.0);
	itens.put(item1, 1);
	itens.put(item2, 1);
	itens.put(item3, 1);
	itens.put(item4, 1);
	Double total = 0d;
	for (Item item : itens.keySet()) {
		total += item.getValorUnitario() * itens.get(item);
	}
	return total;
}


}

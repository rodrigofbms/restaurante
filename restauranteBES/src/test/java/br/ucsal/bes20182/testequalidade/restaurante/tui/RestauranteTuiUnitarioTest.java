package br.ucsal.bes20182.testequalidade.restaurante.tui;

import org.junit.*;

import br.ucsal.bes20182.testequalidade.restaurante.dubles.ObterInteiroMock;

import java.io.*;
import org.junit.Assert;

public class RestauranteTuiUnitarioTest {

	// M�todo a ser testado: public Integer obterInteiro(String mensagem)
	// Verificar o processo de entrada e sa�da para obten��o de um n�mero
	// inteiro. Obs: lembrar de fazer o mock do System.in e do System.out.
	private final InputStream systemIn = System.in;
	private final PrintStream systemOut = System.out;
	private ByteArrayInputStream in;
	private ByteArrayOutputStream out;
	
	@Before
	public void setup() {
		out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
	}
	
	private void entrada (String mensagem) {
		in = new ByteArrayInputStream(mensagem.getBytes());
		System.setIn(in);
	}
	
	private String saida() {
		return out.toString();
	}
	
	@Test
	public void verificarObterInteiro() throws Exception {
		String mensagem = "3";
		entrada(mensagem);
		ObterInteiroMock.main(new String[0]);
		
		Assert.assertEquals(mensagem, saida());
		
		

	}
}
